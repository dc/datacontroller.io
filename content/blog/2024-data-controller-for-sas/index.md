---
title: Data Controller - 2024 Recap
description: A recap of the features delivered in Data Controller for SAS over the 24 releases of 2024
date: '2024-12-31 12:00:00'
author: 'Allan Bowe'
authorLink: https://www.linkedin.com/in/allanbowe/
previewImg: './darkmode.png'
tags:
  - Data Controller
  - SAS
  - Excel
---

Today marks the last day of 2024 and another period of growth for Data Controller for SAS. We made **24** [releases](https://git.datacontroller.io/dc/dc/releases) over the last 12 months, moving from [v6.3.1](https://git.datacontroller.io/dc/dc/releases/tag/v6.3.1) to [v6.12.1](https://git.datacontroller.io/dc/dc/releases/tag/v6.12.1).  We also on-boarded three new clients - two banks,  and an insurance company.  Every single one of our existing customers renewed.  And some even invested in some new features.  In this article we'll go through the major additions made during 2024.

## Complex Excel Uploads

A Basel III regulatory workstream necessitated ingestion of a series of Quantitative Tables as well as data from other workbooks, that were particularly large and packed with data fields situated in dynamic locations within the workbook.  

To meet the requirements (and more) we built a feature to allow the ingestion of multiple ranges, from multiple worksheets, from within an arbitrary workbook.  The target range can be defined using either a fixed (A1) or dynamic (R1C1) notation.  The sheets themselves may be referenced by name, or position. 

By way of example, consider the ingestion of the yellow squares below:

![](./xlmap_example.png)

One potential configuration (of many) to upload the above data would be as follows (extract from [MPE_XLMAP_RULES](/tables/mpe_xlmap_rules) table):


![](./complexloads.png)


The following video illustrates:

<iframe title="Complex Excel Uploads" width="560" height="315" src="https://vid.4gl.io/videos/embed/3338f448-e92d-4822-b3ec-7f6d7530dfc8" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups allow-forms"></iframe>


## Filter By Variable

Since [v6.5](https://git.datacontroller.io/dc/dc/releases/tag/v6.5.0) it is now possible to use variables instead of _values_ on the right hand side of a query expression.  This provides more flexibility in creating filters.

![](./byvar.png)

## Data Restore

Since adding the [audit](https://docs.datacontroller.io/tables/mpe_audit/) data last year, it is now possible (since [v6.8](https://git.datacontroller.io/dc/dc/releases/tag/v6.8.0)) to [restore](https://docs.datacontroller.io/restore/) a table to a particular previous version!

This works by navigating first to the submission of the version to be restored, and hitting the red REVERT button.  This will make a NEW submission with the changes that need to be made to revert the values to the previous state (which needs to be approved in the usual fashion).

![](https://docs.datacontroller.io/img/restore.png)

## Accessibility Updates

It's a hard requirement that Data Controller provides the necessary accessibility adjustments (eg Dark Mode, keyboard shortcuts, contrast / font size settings etc) to accomodate users with particular needs (eg section 508 and EAA).  Thanks to our HandsOnTable licence, the DC data grid will even support for screen readers, such as NVDA, Jaws (Windows) and VoiceOver (MacOS).

![](https://git.datacontroller.io/dc/dc/attachments/09bf644e-63bb-412a-aa50-1617ae08ab2c)

<iframe title="Data Controller Accessibility" width="560" height="315" src="https://vid.4gl.io/videos/embed/b3d7e820-249f-4bc2-9e1a-473ce90a7a96" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups allow-forms"></iframe>

## Multiload Feature

For a long time we have supported the load of a single (rectangular) table from anywhere in a workbook.  Well, as of [v6.11](https://git.datacontroller.io/dc/dc/compare/v6.10.1...v6.11.0) you can now load _multiple_ tables from a single workbook, significantly reducing the effort of manual uploads.  This piece of work also resulted in a big re-write of our excel extraction logic, which is now 20x faster than before.

![](https://git.datacontroller.io/dc/dc/attachments/ebd0b47f-d912-4897-98cf-276a56dda106)


------------

We would like to thank all of our customers for their enthusiasm for and support with our product.  Our goal is to make it secure, simple, and fast to load ad-hoc data into SAS.

If you'd like to kick the tyres, [reach out](https://datacontroller.io/contact/), or just go ahead and perform an [installation](https://docs.datacontroller.io/dci-requirements/) - the base product is completely free to use, with [premium features](https://datacontroller.io/pricing/) / support plan available on request.