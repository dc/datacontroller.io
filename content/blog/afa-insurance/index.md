---
title: AFA Insurance and Data Controller for SAS®
description: Data Controller for SAS helps the DWh team at AFA försäkring with their steering table management
date: '2025-02-25 09:00:00'
author: 'Allan Bowe'
authorLink: https://www.linkedin.com/in/allanbowe/
previewImg: './afa_logo.png'
tags:
  - Use Cases
---


AFA försäkring is a well-known insurance provider in Sweden with a focus on collective agreements and occupational insurance.  

AFA has leveraged SAS for data management and analytics for many years, and recently integrated Data Controller for SAS to enhance controls and audit reporting for a number of business processes.

We caught up with [Fredrik Englund](https://www.linkedin.com/in/fredrik-englund-7402031/) (FE) and [Henrik Forsell](https://www.linkedin.com/in/henrikforsell/) (HF) to learn more about their usage of this toolset.

| ![Fredrik Englund](./fredrik.avif)| 
|:--:| 
| *Fredrik Englund (FE)* |


### Can you tell us a bit about yourself?
(FE) We are the developers of our DWh, a group of around 10 people.  My role is SAS Developer and System Architect.  

### What do you use Data Controller for?
Mainly for steering (mapping, control) tables.  We use it in our handling system for insurance agents – we have a signal system, such that when an agent finishes a task, there is a signal to move forward to the next step in the process.  We map the signals to different process steps and use that in our reports to follow up.  The mapping of signals to process steps are made by end users themselves, using Data Controller, and we make the approvals directly in production without needing fixed programs or anything else.  This is a great advantage for us. 

We have batches running and checking if there are new signals, and we have set up Data Controller with a filter so that end users get only the new signals in the interface, and can just do the mapping and click OK. 

So it is very easy for us now to do that process!

### Why are you using Data Controller, instead of another approach or tool?
We heard about Data Controller from the community, and it seemed like a great fit for our use case - it’s SAS friendly, it’s built on SAS, and it’s easy to incorporate.  

The things we really like are the EDIT and APPROVAL process, and the HISTORY of all changes that is built into the product.

### Is anyone else at AFA using Data Controller?
Not right now but we plan to use it more.  We have people in the process development team who need to change targets once a year or maybe more often.  Right now they send us the new targets and we add them in the programs / update the Visual Analytics reports, but we plan to use steering / control tables for them, so they can update targets themselves in Data Controller.  Then they can request to change them when they like, and we have control over the timing of when the changes are applied. 

We also have some percentages (relating to payments) with levels that change during the year, and those users should also be able to change the limits themselves in the Data Controller interface.

| ![Henrik Forsell](./henrik.jpeg)| 
|:--:| 
| *Henrik Forsell (HF)* |

 
(HF)  This takes us completely out of the equation – we don’t have to do the changes, it’s all on them, they own it.

(FE)  Yes – their data, their controls.  We can leave it to them from the development environment, so they are not dependent on us for any changes, so it’s really good.

(HF)  So the users can own the data, all of these things, they can change it themselves, we don’t need to do anything once we set it up.

### How much time does this save you, say, on a monthly basis?
(FE)  It’s hard to say how much time/money it saves – it’s more about the quality, and the control, and the history.  If they put in wrong mappings, targets, or levels – how much will it cost us to reverse the the batches, it’s very hard to say

(HF)  It’s also about the fact that even if it’s only one number in one report, if we have to do the changes, we’d almost have to setup a small project; as in, a task that has to be checked, added into our test environment, run tests, ok it, and move on into production.  So even though it’s a small thing that changed, the process around it takes a lot of work.  One change might be 3-4 hours in total.  But now with Data Controller, they can change it twice a day if they want, it’s up to them, it doesn’t affect our work at all.

(FE)  If we compare it to the old solution, they did the changes directly in the base tables – in production – so if they added the wrong values it would crash.  Which happened, a few times.  So it’s hard to say but you can spend like 10 hours on finding the error, or maybe more.

(HF)  Or having to do a rollback, and then you involve even more people

(FE)  It’s much more about quality.  It can save us 2-10 hours a month, hard to say.

### What are your favourite features of DC?
(FE)  The UI is simple to understand (in a good way).  It’s very responsive, very quick.  For us the APPROVAL part is the big thing, together with the history of all changes.  It’s really good to see who changes what, and when.  We have a few people who can do the approval, and they can check very thoroughly that it’s correct before they approve it.  So that’s actually the big one.

Then another good / big thing is the [dropdowns](https://docs.datacontroller.io/dynamic-cell-dropdown/), such that you can provide only values that are valid.  And you can do [HOOKS](https://docs.datacontroller.io/dcc-tables/#pre_edit_hook) to check the data, both after and before.  

Another was the [data lineage](https://docs.datacontroller.io/dcu-lineage/), which we haven’t had before.  Now we can have analytics users check the lineage.  We didn’t expect that but it was a nice thing to have.

### Is there anything you’d like to add?
(HF)  We love the response time from the developers. 

(FE)  The support is fabulous.  If we can have this from all of our vendors it would be a perfect world.

(HF)  And it is very important.  When we found something – no words – you are there.  That helps a lot.

(FE)  Also, we haven’t tried out all the [excel import](https://docs.datacontroller.io/excel/) features yet but we’d like to try that in some way, as there is a lot of data that exists in excel, and that can be a way for business users to import that data, and check it, and improve it.  So - we have plans.

--- 
The team behind Data Controller would like to say "STORT TACK" to Fredrik and Henrik for their kind words! 

---

The previous article in this series is available [here](/allianz-insurance-data-controller-sas/).
